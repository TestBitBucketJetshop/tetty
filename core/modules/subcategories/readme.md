Fetch subcategories per category.
 
This is the responsive version of old Jetpack called "Subcategories in main area"

**Config**

``config.showImages`` If set to false, no images will be displayed, and HasImages in template will be set to false as well.

**Installation**

Nothing. Just activate the module.