Standard mobile menu 

This is the mobile bar for size medium & small

Please see file ``template.md`` for instructions

**Dependency**

Module `cat-nav-mobile`

**Compatible with**

- ``cat-nav-mobile``

- ``dynamic-cart``

- ``cat-nav-mega-dd``

- ``cat-nav-vert-down``

- ``cat-nav-vert-right``

**Category navigation dependencies**

If this module are removed the following functions need to be re-created:

Class ``.menu-open`` added to ``<html>`` to open and close the cat-nav menu
 
Class ``.cart-open`` added to ``<html>`` to open and close the cart

Class ``.search-open`` added to ``<html>`` to open and close the search box
 
File ``core/pages/base.master.htm`` need reversion, see ``template.md``
