var breadcrumb = {
    init: function () {
        if (!J.checker.isStage && !J.checker.isCheckoutPage){
            J.api.startpage.list(breadcrumb.render, null, true, 30);
        }
    },
    render: function (data) {
        if (data.length) {
            for (var key in data){
                if (data[key]["IsActive"]){
                    var pageName = data[key]["PageName"];
                    $("#path-nav").prepend('<a class="breadcrumb-link" href="' + J.config.urls.CountryRootUrl + '">'+ pageName +'</a><span class="breadcrumb-spacer"> &gt; </span>');
                }
            }
        }
    }
};

J.pages.addToQueue("all-pages", breadcrumb.init);
