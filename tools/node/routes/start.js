//
//  GET /start page.
//
var fs = require("fs");
var path = require("path");
require("../node-functions.js");

exports.index = function (req, res) {
    res.render('start', {
        layout: false,
        packageData: getPackageJson(),
        activeModules: getActiveModules()
    });
};

