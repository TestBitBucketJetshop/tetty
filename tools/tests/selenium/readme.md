# Instructions for running Selenium tests

1. Set ``http://foundation2.jetshop.se/`` as Base URL in Selenum

1. Run ``test-suite.html`` in Selenium IDE

[Download Selenium IDE (Firefox plugin)](https://addons.mozilla.org/en-US/firefox/addon/selenium-ide/)

> Note: As of now, this test will only work against http://foundation2.jetshop.se

> All tests against API will fail on other domains

### Test-suites available

``test-suite-full.html`` Runs all available tests, including tests from activated modules. In order to create this file run grunt task ``grunt create-tests``

``test-suite.html`` Runs all available tests, excluding modules.

``test-suite-basic.html`` Basic test, not testing full functionality.


### Required setup in Admin (not complete):


1. Make sure that the shop uses the Responsive checkout

1. Add a category with name: ``Bikes``, url: ``bikes``. Re-order categories and put category Bikes as the first one.

1. Add a product with name: `Test``, url: ``test``, price: ``10.000EK``

1. Add product ``Hipster Bike`` to category ``Bikes``.

1. Add a product with name: ``Hipster Bike``, url: ``hipster-bike``, price: 500 SEK

1. Add related product: Main product ``Test``, related poduct ``Hipster Bike``

1. Add news-item with name and url ``test-nyhet``

1. Edit category ``Visningmall Standard`` to use display template ``Listning med bild avancerad`` 

1. Add two manufacturers. Name and URL are not important, just create two of them.

1. Edit the first manufacturer (lowest id) to use display template ``Listning med bild avancerad`` 
